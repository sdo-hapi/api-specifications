## Where to access

You need to follow instructions from your provider's API portal or platform.

For ISO, sign-up for an account at our [Developer Portal](https://api-portal.iso.org).


## General documentation

In the [project's wiki](https://bitbucket.org/sdo-hapi/api-specifications/wiki/Home).
