# Harmonized Interfaces API Specifications

## Specification format

The Harmonized Interfaces project uses the standard OpenAPI specification format.
The well known Swagger technology implements OpenAPI which is now endorsed by major organizations. 

Specifications come as standalone files in the YAML format. 
Our files are located in the "openapi-v3" directory. 

If you spot an error or wish to propose changes, you may create a GIT pull request and send it to us.

## Editing the OpenAPI files

#### Online

The Swagger web site offers an [online browser / editor](https://editor.swagger.io/)
for the OpenAPI specification files. By default you browse and edit their sample "Petstore"
API but you may copy / paste or import our API specifications and start using them.

#### Offline

You may install a local version of the swagger editor on your computer and configure 
it to load your file from your cloned GIT repository. 
The editor's installer is located at [github](https://github.com/swagger-api/swagger-editor)
