openapi: 3.0.0
servers:
  - url: https://api.iec.ch/harmonized
  - url: https://api-test.iec.ch/harmonized
info:
  description: >-
    Please see https://bitbucket.org/sdo-hapi/data-models for components model
  version: 1.0.0
  title: Harmonized API for Ballots
tags:
  - name: Ballots
    description: >-
      API to retrieve Ballots
paths:
  /ballots:
    get:
      tags:
        - Ballots
      summary: >-
        List ballots
      operationId: listBallots
      parameters:
        - name: originator
          in: query
          description: name of the organization issuing ballot, i.e. IEC, CENELEC. If it's not given, the response returns ballots issued by IEC and Cenelec for European Members of IEC and Cenelec
          required: false
          allowEmptyValue: false
          schema:
            type: string
          example: 'CENELEC'
        - name: projectUrn
          in: query
          description: projectUrn related to a ballot (i.e. 'cenelec:proj:74789')
          required: false
          allowEmptyValue: false
          schema:
            type: string
          example: 'cenelec:proj:74789'
        - name: committeeUrn
          in: query
          description: committeeUrn related to a ballot (i.e. 'iec:committee:4911')
          required: false
          allowEmptyValue: false
          schema:
            type: string
          example: 'iec:committee:4911'
        - name: type
          in: query
          description: >-
            Ballot type
          required: false
          allowEmptyValue: false
          schema:
            type: string
            enum: ['Enquiry','parallel vote on cdv','parallel vote on fdis', 'formal vote', 'vote on new work item','vote for ts/tr','Committee Internal Voting']
          example: 'parallel vote on fdis'
        - name: status
          in: query
          description: >-
            Listing all ballots with a status
          required: false
          allowEmptyValue: false
          schema:
            type: string
            enum: ['ACTIVE', 'CLOSED', 'DELETED']
          example: 'CLOSED'
        - name: reference
          in: query
          description: >-
            ballot's reference, exact match (i.e 'IEC 62057-3 ED1 (13/1916/FDIS) (EQV)')
          required: false
          allowEmptyValue: false
          schema:
            type: string
        - name: harmonizedStage
          in: query
          description: harmonizedStage (i.e. 10, 20, 30, 40, 50, 60 or 95). Please see https://www.iso.org/stage-codes.html).
          required: false
          allowEmptyValue: false
          schema:
            type: string
        - name: lastChangeTimestampFrom
          in: query
          description: lastChangeTimestamp from at (i.e. 2024-05-01)
          required: false
          allowEmptyValue: false
          schema:
            type: string
            format: date without timezone, UTC is the default timezone (e.g.'2024-05-01')
        - name: lastChangeTimestampTo
          in: query
          description: lastChangeTimestamp to at (i.e. 2024-05-31)
          required: false
          allowEmptyValue: false
          schema:
            type: string
            format: date without timezone, UTC is the default timezone (e.g. 2024-05-31)
        - name: openingDueDateFrom
          in: query
          description: lifecycle.opening.dueDate from at (i.e. 2024-05-01)
          required: false
          allowEmptyValue: false
          schema:
            type: string
            format: date without timezone, UTC is the default timezone (e.g. 2024-05-31)
        - name: openingDueDateTo
          in: query
          description: lifecycle.opening.dueDate to at (i.e. 2024-05-31)
          required: false
          allowEmptyValue: false
          schema:
            type: string
            format: date without timezone, UTC is the default timezone (e.g. 2024-05-31)
        - name: openingEffectiveDateFrom
          in: query
          description: lifecycle.opening.effectiveDate from at (i.e. 2024-05-01)
          required: false
          allowEmptyValue: false
          schema:
            type: string
            format: date without timezone, UTC is the default timezone (e.g. 2024-05-31)
        - name: openingEffectiveDateTo
          in: query
          description: lifecycle.opening.effectiveDate to at (i.e. 2024-05-31)
          required: false
          allowEmptyValue: false
          schema:
            type: string
            format: date without timezone, UTC is the default timezone (e.g. 2024-06-01)
        - name: closingVoteDueDateFrom
          in: query
          description: lifecycle.closingVote.dueDate from at (i.e. 2024-05-01)
          required: false
          allowEmptyValue: false
          schema:
            type: string
            format: date without timezone, UTC is the default timezone (e.g. 2024-05-01)
        - name: closingVoteDueDateTo
          in: query
          description: lifecycle.closingVote.dueDate to at (i.e. 2024-05-31)
          required: false
          allowEmptyValue: false
          schema:
            type: string
            format: date without timezone, UTC is the default timezone (e.g. 2024-05-31)
        - name: closingVoteEffectiveDateFrom
          in: query
          description: lifecycle.closingVote.effectiveDate from at (i.e. 2024-05-31)
          required: false
          allowEmptyValue: false
          schema:
            type: string
            format: date without timezone, UTC is the default timezone (e.g. 2024-05-01)
        - name: closingVoteEffectiveDateTo
          in: query
          description: lifecycle.closingVote.effectiveDate to at (i.e. 2024-05-31)
          required: false
          allowEmptyValue: false
          schema:
            type: string
            format: date without timezone, UTC is the default timezone (e.g. 2024-05-31)
        - name: page
          in: query
          description: page number (i.e. 5)
          required: false
          allowEmptyValue: false
          schema:
            type: integer
            format: int32
            default: 0
        - name: size
          in: query
          description: number of ballots to return (i.e. 10)
          required: false
          allowEmptyValue: false
          schema:
            type: integer
            format: int32
            default: 100
      responses:
        '200':
          description: OK
        '400':
          description: Bad search parameters

  '/ballots/{urn}':
    get:
      tags:
        - Ballot
      summary: >-
        Access a specific ballot by its urn
      operationId: getBallot
      parameters:
        - name: urn
          in: path
          description: urn
          required: true
          schema:
            type: string
          example: iec:ballot:121449
      responses:
        '200':
          description: OK
        '404':
          description: Ballot not found

  '/ballots/configurations':
    get:
      tags:
        - Ballot configuration
      summary: >-
        List ballot configurations
      operationId: listBallotConfiguration
      parameters:
        - name: originator
          in: query
          description: name of the organization related to ballot configuration, i.e. IEC, CENELEC.
          required: true
          allowEmptyValue: false
          schema:
            type: string
          example: 'CENELEC'
      responses:
        '200':
          description: OK

components:
  securitySchemes:
    bearerAuth:            # arbitrary name for the security scheme
      type: http
      scheme: bearer
security:
  - bearerAuth: []
