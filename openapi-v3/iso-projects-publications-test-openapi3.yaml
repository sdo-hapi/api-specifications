openapi: 3.0.0
servers:
  - url: 'https://api-test.iso.org/harmonized'
info:
  description: ISO TEST Harmonized Projects and Publications. Please see https://bitbucket.org/sdo-hapi/data-models for components model
  version: 1.5.0
  title: Harmonized Projects and Publications for ISO
tags:
  - name: Projects
    description: Projects API
  - name: Publications
    description: Publications API
paths:
  /projects:
    get:
      tags:
        - Projects
      summary: >-
        List and filter projects by different query parameters ordered by date
        desc
      operationId: getProjectsUsingGET
      parameters:
        - name: lastChangeTimestampFrom
          in: query
          description: lastChangeTimestamp from at (i.e. 2018-01-01)
          required: false
          allowEmptyValue: false
          schema:
            type: string
            format: date with/without timezone or relative time to now (eg. 3d, 1w, 3m)
        - name: lastChangeTimestampTo
          in: query
          description: lastChangeTimestamp to at (i.e. 2018-05-31)
          required: false
          allowEmptyValue: false
          schema:
            type: string
            format: date with/without timezone or relative time to now (eg. 3d, 1w, 3m)
        - name: reference
          in: query
          description: project's reference, exact match. If value ends with "*" a startWith match is performed
          required: false
          allowEmptyValue: false
          schema:
            type: string
        - name: stdNumber
          in: query
          description: project's standard or document "number", which may also contain non-numeric characters, e.g. "9001"
          required: false
          allowEmptyValue: false
          schema:
            type: string
        - name: stdPartNumber
          in: query
          description: project's standard or document part "number", which may also contain non-numeric characters, e.g. "2B"
          required: false
          allowEmptyValue: false
          schema:
            type: string
        - name: harmonizedStageCode
          in: query
          description: harmonizedStageCode (i.e. 60.60) cannot be used with stageCode. This will match any "CURRENT" stage, not a "CLOSED" stage.
          required: false
          allowEmptyValue: false
          schema:
            type: string
        - name: stageCode
          in: query
          description: stageCode (i.e. 6060) cannot be used with harmonizedStageCode
          required: false
          allowEmptyValue: false
          schema:
            type: string
        - name: stageDueDateFrom
          in: query
          description: stage dueDate From for PLANNED status (i.e. 2018-01-01)
          required: false
          allowEmptyValue: false
          schema:
            type: string
            format: date with/without timezone or relative time to now (eg. 3d, 1w, 3m)
        - name: stageDueDateTo
          in: query
          description: stage dueDate To for PLANNED status(i.e. 2018-05-31)
          required: false
          allowEmptyValue: false
          schema:
            type: string
            format: date with/without timezone or relative time to now (eg. 3d, 1w, 3m)
        - name: stageEffectiveDateFrom
          in: query
          description: >-
            stage effectiveDate starting with status default CURRENT starting at
            (i.e. 2018-01-01)
          required: false
          allowEmptyValue: false
          schema:
            type: string
            format: date with/without timezone or relative time to now (eg. 3d, 1w, 3m)
        - name: stageEffectiveDateTo
          in: query
          description: >-
            stage effectiveDate ending with status default CURRENT ending at
            (i.e. 2018-05-31)
          required: false
          allowEmptyValue: false
          schema:
            type: string
            format: date with/without timezone or relative time to now (eg. 3d, 1w, 3m)
        - name: stageStatus
          in: query
          description: >-
            stage status (values CURRENT, CLOSED, PLANNED), default CURRENT when
            a stage (effective, due, target) date is filled else empty
          required: false
          allowEmptyValue: false
          schema:
            type: string
            enum:
              - CLOSED
              - CURRENT
              - PLANNED
        - name: stageTargetDateFrom
          in: query
          description: stage targetDate From for PLANNED status(i.e. 2018-01-31)
          required: false
          allowEmptyValue: false
          schema:
            type: string
            format: date with/without timezone or relative time to now (eg. 3d, 1w, 3m)
        - name: stageTargetDateTo
          in: query
          description: stage targetDate To for PLANNED status(i.e. 2018-05-31)
          required: false
          allowEmptyValue: false
          schema:
            type: string
            format: date with/without timezone or relative time to now (eg. 3d, 1w, 3m)
        - name: sortBy
          in: query
          description: Name of the field to sort results by, e.g. "lastChangeTimestamp", "urn"
          required: false
          allowEmptyValue: false
          schema:
            type: string
            enum:
              - lastChangeTimestamp
              - urn
        - name: page
          in: query
          description: page number (i.e. 5)
          required: false
          allowEmptyValue: false
          schema:
            type: integer
            format: int32
            default: 0
        - name: size
          in: query
          description: number of publications to return (i.e. 10)
          required: false
          allowEmptyValue: false
          schema:
            type: integer
            format: int32
            default: 10
        - name: committeeUrn
          in: query
          description: committee urn, exact 'value'. (i.e. committeeUrn=iso:committee:54158&committeeUrn=iso:committee:48138&committeeUrn=iso:committee:4559)
          required: false
          allowEmptyValue: false
          schema:
            type: array
            items:
              type: string
        - name: committeeDisplayName
          in: query
          description: committee display name, exact 'value'. (i.e. committeeDisplayName=ISO/TC%2022&committeeDisplayName=ISO/TC%2061/SC%2012)
          required: false
          allowEmptyValue: false
          schema:
            type: array
            items:
              type: string
        - name: projectRelationType
          in: query
          description: project relation type, exact 'value'. (i.e. projectRelationType=ADOPTED_FROM&projectRelationType=JOINED_WORK_ITEM&projectRelationType=VIENNA_AGREEMENT)
          required: false
          allowEmptyValue: false
          schema:
            type: array
            items:
              type: string
        - name: externalProjectId
          in: query
          description: externalProjectId (i.e. iso:proj:21494) - @ISO Not used
          required: false
          allowEmptyValue: false
          schema:
            type: string
      responses:
        '200':
          description: OK
        '400':
          description: Bad request (parameters syntax or semantics)
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found
  '/projects/{urn}':
    get:
      tags:
        - Projects
      summary: Access a specific project by its urn
      operationId: getProjectUsingGET
      parameters:
        - name: urn
          in: path
          description: urn
          required: true
          schema:
            type: string
      responses:
        '200':
          description: Project found
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Project not found
  /publications:
    get:
      tags:
        - Publications
      summary: >-
        List and filter publications by different query parameters ordered by
        date desc
      operationId: getPublicationsUsingGET
      parameters:
        - name: projectUrn
          in: query
          description: urn of the project associated to the publications to search for, e.g. iso:proj:62085
          required: false
          allowEmptyValue: false
          schema:
            type: string
        - name: lastChangeTimestampFrom
          in: query
          description: lastChangeTimestamp starting at (i.e. 2018-01-01)
          required: false
          allowEmptyValue: false
          schema:
            type: string
            format: date with/without timezone or relative time to now (eg. 3d, 1w, 3m)
        - name: lastChangeTimestampTo
          in: query
          description: lastChangeTimestamp ending at (i.e. 2018-05-31)
          required: false
          allowEmptyValue: false
          schema:
            type: string
            format: date with/without timezone or relative time to now (eg. 3d, 1w, 3m)
        - name: reference
          in: query
          description: project's reference, exact match. If value ends with "*" a startWith match is performed
          required: false
          allowEmptyValue: false
          schema:
            type: string
        - name: stdNumber
          in: query
          description: project's standard or document "number", which may also contain non-numeric characters, e.g. "9001"
          required: false
          allowEmptyValue: false
          schema:
            type: string
        - name: stdPartNumber
          in: query
          description: project's standard or document part "number", which may also contain non-numeric characters, e.g. "2B"
          required: false
          allowEmptyValue: false
          schema:
            type: string
        - name: publicationStage
          in: query
          description: Publication stage, exact 'value'. The possible values are "CD", "DIS", "FDIS" and "IS". (i.e. publicationStage=IS)
          required: false
          allowEmptyValue: false
          schema:
            type: string
        - name: releaseItemReleaseDateFrom
          in: query
          description: lower bound of interval to search property "releaseItems.releaseDate"
          required: false
          allowEmptyValue: false
          schema:
            type: string
            format: date with/without timezone or relative time to now (eg. 3d, 1w, 3m)
        - name: releaseItemReleaseDateTo
          in: query
          description: upper bound of interval to search property "releaseItems.releaseDate"
          required: false
          allowEmptyValue: false
          schema:
            type: string
            format: date with/without timezone or relative time to now (eg. 2020-11-13)
        - name: releaseItemFormat
          in: query
          description: format file to search in property "releaseItems.format" (i.e. PDF)
          required: false
          allowEmptyValue: false
          schema:
            type: array
            items:
              type: string
        - name: contentLanguage
          in: query
          description: language to search in property "releaseItem.contentLanguage", exact 'value'. (i.e. en)
          required: false
          allowEmptyValue: false
          schema:
            type: array
            items:
              type: string
        - name: committeeUrn
          in: query
          description: committee urn, exact 'value'. (i.e. iso:committee:45144)
          required: false
          allowEmptyValue: false
          schema:
            type: array
            items:
              type: string
        - name: committeeReference
          in: query
          description: committee reference, 'regex expression'. (i.e. ISO/TC 4/.*)
          required: false
          allowEmptyValue: false
          schema:
            type: array
            items:
              type: string
        - name: publicationDateFrom
          in: query
          description: lower bound of interval to search property "publicationDate"
          required: false
          allowEmptyValue: false
          schema:
            type: string
            format: date with/without timezone or relative time to now (eg. 3d, 1w, 3m)
        - name: publicationDateTo
          in: query
          description: upper bound of interval to search property "publicationDate"
          required: false
          allowEmptyValue: false
          schema:
            type: string
            format: date with/without timezone or relative time to now (eg. 2021-11-13)
        - name: publicationStatus
          in: query
          description: publication "status", exact 'value'. (i.e. PUBLISHED, DRAFT, WITHDRAWN, IN_DEVELOPMENT)
          required: false
          allowEmptyValue: false
          schema:
            type: array
            items:
              type: string
        - name: classificationType
          in: query
          description: classification type, exact 'value'. (i.e. ICS, SUSTAINABLE_DEVELOPMENT_GOAL, LOGO)
          required: false
          allowEmptyValue: false
          schema:
            type: array
            items:
              type: string
        - name: classificationValue
          in: query
          description: classification value, 'regex expression'. (i.e. 11.* to get Health Standard publications)
          required: false
          allowEmptyValue: false
          schema:
            type: array
            items:
              type: string
        - name: originator
          in: query
          description: originator, exact 'value'. The possible values are "ISO", "IEC/IEEE", "ISO/IEC", "IEC", ...
          required: false
          allowEmptyValue: false
          schema:
            type: string
        - name: sortBy
          in: query
          description: Name of the field to sort results by, e.g. "lastChangeTimestamp", "urn"
          required: false
          allowEmptyValue: false
          schema:
            type: string
            enum:
              - lastChangeTimestamp
              - urn
        - name: page
          in: query
          description: page number (i.e. 5)
          required: false
          allowEmptyValue: false
          schema:
            type: integer
            format: int32
            default: 0
        - name: size
          in: query
          description: number of publications to return (i.e. 10)
          required: false
          allowEmptyValue: false
          schema:
            type: integer
            format: int32
            default: 10
      responses:
        '200':
          description: OK
        '400':
          description: Bad request (parameters syntax or semantics)
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Not Found
  '/publications/{urn}':
    get:
      tags:
        - Publications
      summary: Access a specific publication by its urn
      operationId: getPublicationUsingGET
      parameters:
        - name: urn
          in: path
          description: urn
          required: true
          schema:
            type: string
      responses:
        '200':
          description: Publication found
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Publication not found
components:
  securitySchemes:
    bearerAuth: # arbitrary name for the security scheme
      type: http
      scheme: bearer
security:
  - bearerAuth: []
