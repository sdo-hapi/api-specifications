openapi: 3.0.0
servers:
  - url: https://api.iso.org/harmonized
  - url: https://api-test.iso.org/harmonized
info:
  description: >-
    Please see
    https://bitbucket.org/sdo-hapi/data-models for components model
  version: 1.0.0
  title: Harmonized API for Meetings
tags:
  - name: Meetings
    description: API to retrieve Meetings data
paths:
  /meetings:
    get:
      tags:
        - Meetings
      summary: >-
        List meetings by different query parameters
      operationId: listMeetings
      parameters:
        - name: committeeUrn
          in: query
          description: urn of the Committee that originated the meeting. Format is "iso:committee:<id>" with the Global Directory committee id
          required: false
          allowEmptyValue: false
          schema:
             type: string
          example: 'iso:committee:54018'
        - name: includeSCs
          in: query
          description: include the meetings of the subcommittees of the committee
          required: false
          allowEmptyValue: false
          schema:
            type: boolean
          example: true
        - name: includeWGs
          in: query
          description: include the meetings of the committees working groups
          required: false
          allowEmptyValue: false
          schema:
            type: boolean
          example: true
        - name: status
          in: query
          description: Listing all meetings with a given status. Possible values are PROPOSED, CONFIRMED, REGISTRATION_OPEN, REGISTRATION_DEADLINE, ONGOING, CLOSED, CANCELED, ARCHIVED.
          required: false
          allowEmptyValue: false
          schema:
            type: string
            enum: [PROPOSED, CONFIRMED, REGISTRATION_OPEN, REGISTRATION_DEADLINE, ONGOING, CLOSED, CANCELED, ARCHIVED]
          example: REGISTRATION_OPEN
        - name: from
          in: query
          description: meeting starting from  (i.e. 2018-01-01, 1d, 1h, 2w)
          required: false
          allowEmptyValue: false
          schema:
            type: string
            format: date with/without timezone
          example: '2015-05-01'
        - name: to
          in: query
          description: to (i.e. 2018-05-31)
          required: false
          allowEmptyValue: false
          schema:
            type: string
            format: date with/without timezone
          example: '2022-05-02'
        - name: countryUrn
          in: query
          description: urn of the country where the meeting is occurring. Format is iso:country:<id>
          required: false
          allowEmptyValue: false
          schema:
            type: string
          example: 'iso:country:192'
        - name: presenceType
          in: query
          description: search by presence type. Possible values are FACE_TO_FACE, HYBRID or VIRTUAL
          required: false
          allowEmptyValue: false
          schema:
            type: string
            enum: [FACE_TO_FACE, HYBRID, VIRTUAL]
          example: HYBRID
        - name: town
          in: query
          description: search by city
          required: false
          allowEmptyValue: false
          schema:
            type: string
          example: Geneva
        - name: lastChangeTimestampFrom
          in: query
          description: lastChangeTimestamp from at (i.e. 2018-01-01, 1d, 1h, 2w)
          required: false
          allowEmptyValue: false
          schema:
            type: string
            format: date with/without timezone or relative time to now (eg. 3d, 1w, 3m)
        - name: lastChangeTimestampTo
          in: query
          description: lastChangeTimestamp to at (i.e. 2018-05-31)
          required: false
          allowEmptyValue: false
          schema:
            type: string
            format: date with/without timezone or relative time to now (eg. 3d, 1w, 3m)
        - name: lastParticipationChangeTimestampFrom
          in: query
          description: lastParticipationChangeTimestamp from at (i.e. 2018-01-01, 1d, 1h, 2w)
          required: false
          allowEmptyValue: false
          schema:
            type: string
            format: date with/without timezone or relative time to now (eg. 3d, 1w, 3m)
        - name: lastParticipationChangeTimestampTo
          in: query
          description: lastParticipationChangeTimestamp to at (i.e. 2018-05-31)
          required: false
          allowEmptyValue: false
          schema:
            type: string
            format: date with/without timezone or relative time to now (eg. 3d, 1w, 3m)
        - name: page
          in: query
          description: page number (i.e. 5)
          required: false
          allowEmptyValue: false
          schema:
            type: integer
            format: int32
            default: 0
        - name: size
          in: query
          description: number of meetings to return (i.e. 10)
          required: false
          allowEmptyValue: false
          schema:
            type: integer
            format: int32
            default: 10
        - name: sortBy
          in: query
          description: Name of the field to sort results by. Possible values are DATE_ASC (default), DATE_DESC, COMMITTEE or STATUS
          required: false
          allowEmptyValue: false
          schema:
            type: string
            enum:
              - COMMITTEE
              - STATUS
              - DATE_ASC
              - DATE_DESC
          example: DATE_ASC
      responses:
        '200':
          description: OK
        '400':
          description: Bad request (parameters syntax or semantics)
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
  '/meetings/{urn}':
    get:
      tags:
        - Meetings
      summary: Access a specific meeting by its urn
      operationId: getMeetingUsingGET
      parameters:
        - name: urn
          in: path
          description: urn
          required: true
          schema:
            type: string
      responses:
        '200':
          description: Meeting found
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Meeting not found
components:
  securitySchemes:
    bearerAuth:            # arbitrary name for the security scheme
      type: http
      scheme: bearer
security:
  - bearerAuth: []