openapi: 3.0.0
servers:
  - url: 'https://api-test.cen.eu/harmonized'
info:
  description: CEN TEST Committee Working Documents. Please see https://bitbucket.org/sdo-hapi/data-models for components model
  version: 1.1.2
  title: Harmonized Committee and Working Documents for CEN
tags:
  - name: Documents
    description: Documents API
paths:
  /documents:
    get:
      tags:
        - Documents
      summary: >-
        List and filter documents by different query parameters ordered by date
        desc
      operationId: searchDocuments
      parameters:
        - name: lastChangeTimestampFrom
          in: query
          description: lower bound of interval to search property "lastChangeTimestamp"
          required: false
          allowEmptyValue: false
          schema:
            type: string
            format: date with timezone or relative time to now (eg. 2h, 3d, 1w, 3m)
        - name: lastChangeTimestampTo
          in: query
          description: upper bound of interval to search property "lastChangeTimestamp"
          required: false
          allowEmptyValue: false
          schema:
            type: string
            format: date with timezone or relative time to now (eg. 2h, 3d, 1w, 3m)
        - name: dueDateFrom
          in: query
          description: lower bound of interval to search property "dueDate". @ISO not available yet.
          required: false
          allowEmptyValue: false
          schema:
            type: string
            format: date with timezone or relative time to now (eg. 2h, 3d, 1w, 3m)
        - name: dueDateTo
          in: query
          description: upper bound of interval to search property "dueDate". @ISO not available yet.
          required: false
          allowEmptyValue: false
          schema:
            type: string
            format: date with timezone or relative time to now (eg. 2h, 3d, 1w, 3m)
        - name: committeeUrn
          in: query
          description: urn of the Committee that originated the document. Format is iso:committee:<id> with the Global Directory committee id
          required: false
          allowEmptyValue: false
          schema:
            type: string
        - name: sortBy
          in: query
          description: Name of the field to sort results by, e.g. "lastChangeTimestamp", "urn"
          required: false
          allowEmptyValue: false
          schema:
            type: string
            enum:
              - lastChangeTimestamp
              - urn
        - name: page
          in: query
          description: pagination page number (e.g. 5)
          required: false
          allowEmptyValue: false
          schema:
            type: integer
            format: int32
            default: 0
        - name: size
          in: query
          description: pagination page size = number of items to return (e.g. 10)
          required: false
          allowEmptyValue: false
          schema:
            type: integer
            format: int32
            default: 10
      responses:
        '200':
          description: OK
        '400':
          description: Bad request (parameters syntax or semantics)
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
  '/documents/{urn}':
    get:
      tags:
        - Documents
      summary: Access a specific document by its urn
      operationId: lookupDocument
      parameters:
        - name: urn
          in: path
          description: urn
          required: true
          schema:
            type: string
      responses:
        '200':
          description: Document found
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
        '404':
          description: Document not found
  /documents/search/deleted:
    get:
      tags:
        - Documents
      summary: >-
        Search for deleted documents within date intervals
      operationId: searchDeletedDocuments
      parameters:
        - name: deletedTimestampFrom
          in: query
          description: lower bound of interval to search for deleted documents
          required: false
          allowEmptyValue: false
          schema:
            type: string
            format: date with timezone or relative time to now (eg. 2h, 3d, 1w, 3m)
        - name: deletedTimestampTo
          in: query
          description: upper bound of interval to search for deleted documents
          required: false
          allowEmptyValue: false
          schema:
            type: string
            format: date with timezone or relative time to now (eg. 2h, 3d, 1w, 3m)
        - name: page
          in: query
          description: pagination page number (e.g. 5)
          required: false
          allowEmptyValue: false
          schema:
            type: integer
            format: int32
            default: 0
        - name: size
          in: query
          description: pagination page size = number of items to return (e.g. 10)
          required: false
          allowEmptyValue: false
          schema:
            type: integer
            format: int32
            default: 10
      responses:
        '200':
          description: a source and an array of deleted documents (document urn and date)
          content:
            application/json:
              schema:
                type: object
                required:
                  - source
                  - document
                properties:
                  source:
                    type: object
                    properties:
                      organization:
                        type: string
                      generated:
                        type: string
                        format: datetime
                      schemaVersion:
                        type: string
                  document:
                    type: array
                    items:
                      properties:
                        urn:
                          description: Deleted document urn
                          type: string
                        date:
                          description: Date of deletion
                          type: string
                          format: datetime
              examples:
                json:
                  summary: 'a json sample for result'
                  value: '{
                    "source": {
                      "organization": "ISO",
                      "generated": "2020-10-12T15:27:30.836+02:00",
                      "schemaVersion": "1.1.0"
                    },
                    "document": [
                      {
                        "urn": "cen:doc:b3c46918-9edf-4566-81e9-763533e939e5",
                        "date": "2020-04-28T09:36:27.140+02:00"
                      },
                      {
                        "urn": "cen:doc:9d19b449-5eea-4afa-ae96-f10f1bded986",
                        "date": "2020-04-28T09:36:27.124+02:00"
                      }
                    ]
                  }'
        '400':
          description: Bad request (parameters syntax or semantics)
        '401':
          description: Unauthorized
        '403':
          description: Forbidden
components:
  securitySchemes:
    bearerAuth:            # arbitrary name for the security scheme
      type: http
      scheme: bearer
security:
  - bearerAuth: []
